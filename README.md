# Testing and Unit Testing

##Objectives

Throughout this exercise the students will practice:

* Testing various versions of a program to validate their operation
* Unit Testing

## Concepts

As you have learned in previous labs, getting a program to compile is only a minor part of programming. It is super important to test functions in order to validate that they produce the correct results.


### Exercise 1

This exercise is an adaptation of [1]. You are provided with a program that implements several versions of five simple functions, e.g. rock-paper-scisors, sorting 3 numbers. By testing the versions, you and your partner will determine which of the versions is correctly implemented.   

This first exercise requires **NO programming**, just testing.  For this first exercise, you **do not** have to implement any unit tests.  Just use you testing ability (*by hand*) to determine which function version is the one that is correctly implemented.  

For instance, let us assume that a friend provides you with a program. He claims that his program solves the following problem:

> *"given three different integers, displays the maximum value"*.  

Let's say that the program has an interfaces as follows:

![](http://i.imgur.com/BD6SmKh.png)

**Without analysing the source code**, you could determine if the program provides valid results.  You could try the following cases:

* a = 4, b = 2, c = 1, expect result: 4
* a = 3, b = 6, c = 2, expected result: 6
* a = 1, b = 10, c = 100, expected result: 100

If any of the three cases fails to give the expected result, you friend's program is not working. On the other hand, If all three cases are successful, then the program has a high probability of being correct.

In this exercise, you will be validating various versions of the following functions:

* **3 Sorts**: a program that receives three strings or numbers and sorts them in lexicographical order. For example, given `1`, `3`, and `2`. Will sort them as: `1`, `2`, and `3`.
 
    ![](http://i.imgur.com/FZkbLSJ.png)
  
* **Dice**: when the user presses the `Roll them` button, the program will generate two random numbers between 1 and 6.  The sum of the random numbers is reported. Your task is to determine in which of the versions (if any) did the programmer implement correctly the adding function. 

     ![](http://i.imgur.com/Lxt1oVT.png)

* **Rock, Papers, Scissors**:  Allows each player to choose her play, then reports who won. The interface is shown in the following figure.

    ![](http://i.imgur.com/BbdPPwA.png)


* **Zulu time**: given the time in zulu time (Greenwich Mean Time) and the military zone that the user whishes to know, the program outputs the time at that zone. The format for the input is `####` in 23 hour format, e.g. 2212. The list of valid military zones is given in http://en.wikipedia.org/wiki/List_of_military_time_zones. Examples of correct output:
    * Given Zulu time 1230, the time in zone A (UTC+1) should be 1330
    * Given Zulu time 1230, the time in zone N (UTC-1) should be 1130
    * Puerto Rico is in military zone **Q** (UTC-4), which means that when it is 1800 Zulu time, it is 1400 in Puerto Rico.


    ![](http://i.imgur.com/85dRODC.png)

#### Steps

Step 1: 
Please make sure that you and your partner understand the descriptions of the programs described above. Ask the lab instructor for any further clarifications. For each of the functions described above, write down in your notebook the tests that you will perform in order to determine validity. As you are writing the tests, think about the logic errors that the programmer might have committed.  For each test, write the input values you will provide and the expected result.  

Step 2: 
Download the program from $XYZ$.

Step 3:
Run the program. You will see a window similar to the following:

![](http://demo05.cloudimage.io/s/resize/300/i.imgur.com/ibGORt4.png)

Click the **3 Sorts** button and you will get the following:

![](http://demo05.cloudimage.io/s/resize/300/i.imgur.com/p1NLIdM.png)

The "Version Alpha" combo box indicates that you are running the first version of the 3 Sorts algorithm. Use the tests that you wrote in step 1 to validate "Version Alpha". Then, do the same for versions beta, gamma and delta.  Report which is the correct version in every method. Also, explain the tests that you performed that allowed you to determine that the other versions are incorrect. 

For example, you could organice your results into a table. 

| Num | Test | Alpha | Beta | Gamma | Delta | 
| ------------- | ----------- | ----------- | ----------- | ----------- | ----------- |
| 1| "alce", "coyote", "zorro" | "alce", "coyote", "zorro" | .... | .... |
| 2| "alce", "zorro", "coyote" | "zorro", "alce", "coyote" | .... | .... |


Remember to specify which function version(s) if any were correctly implemented. Also remember to specify which tests allowed you to determine that some versions were incorrectly implemented.

You can see examples of how to organize your results [here](http://i.imgur.com/ggDZ3TQ.png) and [here](http://i.imgur.com/rpApVqm.png).

### Exercise 2

Running tests "by hand" each time that you run a program gets tiresome very quickly. You and your partner have done it for a few simple functions. Imagine doing the same for full-blown program!, such as a browser or word processor.

*Unit tests* help programmers produce valid code and ease the process of debugging while avoiding the tedious task of entering tests by hand on each execution.


Step 1: 
Download the proyect from $XYZ$. The proyect contains only one source code file `main.cpp`. This file contains four functions: `fact`, `isALetter`, `isValidTime`, and `gcd` whose results are only partially correct. Your task is to write unit tests for each of them to identify wrong results.  **You do not need to rewrite the functions to correct them**.

A unit test function `test_fact()` is provided for the function `fact`. If you invoke this function from `main` and compile and run the program. You will get a message like the following:

```cpp
Assertion failed: (fact(2) == 2), function test_fact, file ../UnitTests/main.cpp, line 69.
``` 

This would be enough for us to claim that function `fact` is not correctly implemented. Comment the invocation of `test_fact()` from main.

Step 2:
Write a unit test called `test_isALetter` for the function `isALetter`. In the unit test write several asserts to test some inputs against the expected outputs (look a `test_fact` for inspiration).  Invoke `test_isALetter` from main. Keep performing adding asserts until one of them fails the assertion. Copy the assertion message as part of the deliverables.

Step 3:
Repeat step 2 for the other two functions `isValidTime`, and `gcd`. Remember to call the unit tests from `main` and copy the assertion message that fails the assertion for each of them.


### References

[1] http://nifty.stanford.edu/2005/TestMe/